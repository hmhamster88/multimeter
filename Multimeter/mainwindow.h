#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "multimeter.h"

#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /**
     * @brief Слот для обновления графика
     */
    void updatePlot();

    /**
     * @brief Слот для обновления лога
     */
    void updateLog();

    //Обработчики элементов интерфейса
    void on_graphUpdateSpinBox_valueChanged(int arg1);

    void on_action_triggered();

    void on_logTimeSpinBox_valueChanged(int arg1);

    void on_selectlogFilePushButton_clicked();

    void on_exitAction_triggered();

    void on_action_2_triggered();

    void on_action_3_triggered();

private:
    Ui::MainWindow *ui;

    /**
     * @brief Таймер обновления графика
     */
    QTimer mGraphUpdateTimer;

    /**
     * @brief Таймер обновления лога
     */
    QTimer mLogUpdateTimer;

    /**
     * @brief Класс для работы с мультиметром
     */
    Multimeter mMultimeter;

    /**
     * @brief Последние полученные единицы измерения
     */
    Multimeter::Unit mLastUnits;
};

#endif // MAINWINDOW_H

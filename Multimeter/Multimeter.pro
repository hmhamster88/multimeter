#-------------------------------------------------
#
# Project created by QtCreator 2016-05-17T14:49:19
#
#-------------------------------------------------

QT       += core gui printsupport serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Multimeter
TEMPLATE = app

QMAKE_CXXFLAGS+= -std=c++11
QMAKE_LFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    multimeter.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    multimeter.h \
    qcustomplot.h

FORMS    += mainwindow.ui

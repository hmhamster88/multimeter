#ifndef MULTIMETER_H
#define MULTIMETER_H

#include <QObject>
#include <QSerialPort>
#include <QTimer>

class Multimeter : public QObject
{
    Q_OBJECT
public:
    explicit Multimeter(QObject *parent = 0);
    /**
     * @brief Единицы измерения
     */
    enum Unit
    {
        Unit_V,
        Unit_A,
        Unit_Ohm,
        Unit_Farad,
        Unit_CelsiusDegree,
        Unit_FahrenheitDegree,
        Unit_Hz,
        Unit_Watt
    };

    /**
     * @brief Режим работы мультиметра
     */
    enum Mode
    {
        Mode_Voltage_DC = 0x1,
        Mode_Voltage_AC = 0x2,
        Mode_Voltage_mV = 0x3,
        Mode_Resistance = 0x4,
        Mode_Capacity = 0x5,
        Mode_Temperature_Celsius = 0x6,
        Mode_Current_uA = 0x7,
        Mode_Current_mA = 0x8,
        Mode_Current_A = 0x9,
        Mode_Resistance_WithBeep = 0xA,
        Mode_DiodeJinctionVoltage = 0xb,
        Mode_Frequency = 0xc,
        Mode_Temperature_Fahrenheit = 0xd,
        Mode_Power = 0xe,
        Mode_PercentOf4_20mALoop = 0xf
    };

    /**
     * @brief Возвращает измеряемое значение
     * @return Измеряемое значение
     */
    double value() const;

    /**
     * @brief Возвращает измеряемые единицы
     * @return Измеряемые единицы
     */
    Unit units() const;

    /**
     * @brief Возвращает режим измерений
     * @return Режим измерений
     */
    Mode mode() const;

    /**
     * @brief Устанавливает порт для подключения
     * @param Порт для подключения
     */
    void setPort(const QString& value);

    /**
     * @brief Возвращает статус мультиметра
     * @return Статус мультиметра
     */
    bool isOnline() const;

    /**
     * @brief Возврашает доступные порты
     * @return Доступные порты
     */
    QStringList availablePorts() const;

    /**
     * @brief Возвращает название режима измерния
     * @param Режим измерния
     * @return Название режима измерния
     */
    QString getModeName(Mode mode) const;

    /**
     * @brief Возвращает название единицы измерния
     * @param Единицы измерния
     * @return Название единицы измерния
     */
    QString getUnitName(Unit unit) const;

    /**
     * @brief Возвращает единицы для режима
     * @param Режим измерения
     * @return Единицы измерния
     */
    Unit getUnitFromMode(Mode mode) const;

    /**
     * @brief Возвращает позицию десятичной точки
     * @param Режим измерения
     * @param Данные получаемые с мультиметра
     * @return Позиция десятичной точки
     */
    int getDecimalPointPosition(Mode mode, int range) const;

    /**
     * @brief Возвращает множитель
     * @param Режим измерения
     * @param Данные получаемые с мультиметра
     * @return Множитель
     */
    double getMultiplierForMode(Mode mode, int range) const;

private slots:
    /**
     * @brief Слот для открытия порта
     */
    void tryToConnect();

    /**
     * @brief Слот для чтения данных из порта
     */
    void readyRead();

    /**
     * @brief Слот для разбора пакета
     * @param Буфер
     */
    void parsePacket(const QByteArray& buffer);

    /**
     * @brief Слот для проверки статуса прибора
     */
    void onlineCheck();

    /**
     * @brief Слот для обработки ошибок порта
     * @param Ошибка порта
     */
    void portError(QSerialPort::SerialPortError error);

private:
    /**
     * @brief Статус мультиметра
     */
    bool mIsOnline;

    /**
     * @brief Порт для подключения
     */
    QString mPort;

    /**
     * @brief Измеряемое значение
     */
    double mValue;

    /**
     * @brief Объект для работы с последовательным портом
     */
    QSerialPort mSerialPort;

    /**
     * @brief Таймер для подключения к порту
     */
    QTimer mConnectionTimer;

    /**
     * @brief Таймер для проверки статуса мультиметра
     */
    QTimer mOnlineTimer;

    /**
     * @brief Буфер для полученых данных
     */
    QByteArray mBuffer;

    /**
     * @brief Режим работы мультиметра
     */
    Mode mMode;

    /**
     * @brief Флаг полученого пакета
     */
    bool mPacketReceived;
};

#endif // MULTIMETER_H

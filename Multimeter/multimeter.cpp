#include "multimeter.h"

#include <QSerialPortInfo>

#define PACKET_LENGTH 11

Multimeter::Multimeter(QObject *parent) : QObject(parent),
    mIsOnline(false),
    mValue(0),
    mPacketReceived(false)
{
    // Устанавливаем скорость обмена порта
    mSerialPort.setBaudRate(QSerialPort::Baud2400);
    // Подключаем сигналы и слоты
    connect(&mSerialPort, &QSerialPort::readyRead, this, &Multimeter::readyRead);
    connect(&mSerialPort, static_cast<void(QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &Multimeter::portError);
    connect(&mConnectionTimer, &QTimer::timeout, this, &Multimeter::tryToConnect);
    connect(&mOnlineTimer, &QTimer::timeout, this, &Multimeter::onlineCheck);
    // Запускаем таймеры
    mConnectionTimer.start(1000);
    mOnlineTimer.start(2000);
}

double Multimeter::value() const
{
    return mValue;
}

Multimeter::Unit Multimeter::units() const
{
    return getUnitFromMode(mMode);
}

Multimeter::Mode Multimeter::mode() const
{
    return mMode;
}

void Multimeter::setPort(const QString &value)
{
    // Если порт изменился то закрываем соединение
    if(mPort != value)
    {
        mPort = value;
        if(mSerialPort.isOpen())
        {
            mSerialPort.close();
        }
    }
}

bool Multimeter::isOnline() const
{
    return mIsOnline;
}

QStringList Multimeter::availablePorts() const
{
    QStringList ports;
    for (QSerialPortInfo port : QSerialPortInfo::availablePorts())
    {
        ports.append(port.portName());
    }
    return ports;
}

QString Multimeter::getModeName(Multimeter::Mode mode) const
{
    switch(mode)
    {
    case Mode_Voltage_DC:
        return "Напряжение";
    case Mode_Voltage_AC:
        return "Напряжение";
    case Mode_Voltage_mV:
        return "Напряжение";
    case Mode_Resistance:
        return "Сопротивление";
    case Mode_Capacity:
        return "Ёмкость";
    case Mode_Temperature_Celsius:
        return "Температура";
    case Mode_Current_uA:
        return "Ток";
    case Mode_Current_mA:
        return "Ток";
    case Mode_Current_A:
        return "Ток";
    case Mode_Resistance_WithBeep:
        return "Сопротивление";
    case Mode_DiodeJinctionVoltage:
        return "Диод";
    case Mode_Frequency:
        return "Частота";
    case Mode_Temperature_Fahrenheit:
        return "Температура";
    case Mode_Power:
        return "Мощьность";
    case Mode_PercentOf4_20mALoop:
        return "Токовая петля";
    default:
        return "-";
    }
}

QString Multimeter::getUnitName(Multimeter::Unit unit) const
{
    switch(unit)
    {
    case Unit_V:
        return "В";
    case Unit_A:
        return "А";
    case Unit_Ohm:
        return "Ом";
    case Unit_Farad:
        return "Ф";
    case Unit_CelsiusDegree:
        return "ºC";
    case Unit_FahrenheitDegree:
        return "ºF";
    case Unit_Hz:
        return "Гц";
    case Unit_Watt:
        return "Вт";
    }
    return "-";
}

Multimeter::Unit Multimeter::getUnitFromMode(Multimeter::Mode mode) const
{
    switch(mode)
    {
    case Mode_Voltage_DC:
        return Unit_V;
    case Mode_Voltage_AC:
        return Unit_V;
    case Mode_Voltage_mV:
        return Unit_V;
    case Mode_Resistance:
        return Unit_Ohm;
    case Mode_Capacity:
        return Unit_Farad;
    case Mode_Temperature_Celsius:
        return Unit_CelsiusDegree;
    case Mode_Current_uA:
        return Unit_A;
    case Mode_Current_mA:
        return Unit_A;
    case Mode_Current_A:
        return Unit_A;
    case Mode_Resistance_WithBeep:
        return Unit_Ohm;
    case Mode_DiodeJinctionVoltage:
        return Unit_V;
    case Mode_Frequency:
        return Unit_Hz;
    case Mode_Temperature_Fahrenheit:
        return Unit_FahrenheitDegree;
    case Mode_Power:
        return Unit_Watt;
    case Mode_PercentOf4_20mALoop:
        return Unit_A;
    }
    return Unit_V;
}

int Multimeter::getDecimalPointPosition(Multimeter::Mode mode, int range) const
{
    switch(mode)
    {
    case Mode_Voltage_mV:
        return 3;
    case Mode_Temperature_Celsius:
        return 4;
    case Mode_Temperature_Fahrenheit:
        return 4;
    case Mode_Current_uA:
        return range + 3;
    case Mode_Current_mA:
        return range + 2;
    case Mode_Current_A:
        return 2;
    case Mode_Resistance_WithBeep:
        return 3;
    case Mode_Frequency:
    case Mode_Capacity:
    case Mode_Resistance:
        return (range + 1) % 3 + 1;
    case Mode_DiodeJinctionVoltage:
        return 1;
    case Mode_Power:
        return 4;
    case Mode_PercentOf4_20mALoop:
        return 3;
    default: return range;
    }
    return 0;
}

double Multimeter::getMultiplierForMode(Multimeter::Mode mode, int range) const
{
    switch (mode)
    {
    case Mode_Voltage_mV:
        return 0.001;
    case Mode_Frequency:
    case Mode_Resistance:
        return pow(1000,(range + 1) / 3);
    case Mode_Capacity:
        return pow(1000,(range + 1) / 3) / 1000000000.0;
    default: return 1;
    }
    return 1;
}

void Multimeter::tryToConnect()
{
    // Если соединения нет и строка порта не пустая пытаемся подключиться
    if(!mSerialPort.isOpen() && !mPort.isEmpty())
    {
        mSerialPort.setPortName(mPort);
        mSerialPort.open(QIODevice::ReadWrite);
    }
}

void Multimeter::readyRead()
{
    // Если порт открыт
    if(mSerialPort.isOpen())
    {
        char c;
        // Считываем данные до появления символов 0x0d 0x8a
        while(mSerialPort.read(&c, 1) > 0)
        {
            mBuffer.append(c);
            if(c == (char)0x8a && mBuffer.count() >= PACKET_LENGTH && mBuffer[mBuffer.count() - 2] == '\r')
            {
                // Разбираем пакет
                parsePacket(mBuffer.right(PACKET_LENGTH));
                mBuffer.clear();
            }
        }
    }
}

void Multimeter::parsePacket(const QByteArray &buffer)
{
    mIsOnline = true;
    mPacketReceived = true;
    // Получаем режим работы
    mMode = (Mode)(buffer[6] & 0x0f);
    // Получаем данные для определения десятичной точки и множителя
    int range  = buffer[5] & 0x0f;
    int decimalPointPosition = getDecimalPointPosition(mMode, range);
    // Получаем знак измерения
    bool negative = buffer[8] & (1 << 2);
    const int digitsCount = 5;
    QString num;
    if(negative)
    {
        num += "-";
    }
    // Получаем цифры
    for(int i = 0; i < digitsCount; i++)
    {
        if(i == decimalPointPosition)
        {
            num += '.';
        }
        num += '0' + (buffer[i] & 0x0f);
    }
    // Получаем конечное значение
    mValue = num.toDouble() * getMultiplierForMode(mMode, range);
}

void Multimeter::onlineCheck()
{
    // проверяем статус прибора
    mIsOnline = mPacketReceived;
    mPacketReceived = false;
}

void Multimeter::portError(QSerialPort::SerialPortError error)
{
    // Если произошла ошибка то закрываем порт
    if(error != QSerialPort::NoError)
    {
        if(mSerialPort.isOpen())
        {
            mSerialPort.close();
        }
    }
}

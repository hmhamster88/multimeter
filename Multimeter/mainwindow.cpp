#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Загружаем настройки настроек
    QSettings settings;
    ui->graphUpdateCheckBox->setChecked(settings.value("updateGraph", true).toBool());
    ui->graphUpdateSpinBox->setValue(settings.value("grpaphUpdateTime", 1).toInt());
    ui->writeLogCheckBox->setChecked(settings.value("writeLog", true).toBool());
    ui->logTimeSpinBox->setValue(settings.value("writeLogTime", 1).toInt());
    ui->logFileLineEdit->setText(settings.value("logFile", "log.csv").toString());
    ui->showLastStepsCheckBox->setChecked(settings.value("showLastSteps", true).toBool());
    ui->stepsViewCountSpinBox->setValue(settings.value("stepsViewCount", 10).toInt());

    // Соединяем сигналы таймеров
    connect(&mGraphUpdateTimer, &QTimer::timeout, this, &MainWindow::updatePlot);
    connect(&mLogUpdateTimer, &QTimer::timeout, this, &MainWindow::updateLog);

    // Запускаем таймеры
    mGraphUpdateTimer.start(ui->graphUpdateSpinBox->value() * 1000);
    mLogUpdateTimer.start(ui->logTimeSpinBox->value() * 1000);

    // Настраиваем график
    QCustomPlot* customPlot = ui->qcustomplot;

    customPlot->addGraph();
    customPlot->graph()->setName("График");

    customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    customPlot->xAxis->setDateTimeFormat("hh:mm:ss");

    customPlot->xAxis->setLabel("Время");
    customPlot->yAxis->setLabel("Значение");

    customPlot->setInteraction(QCP::iRangeDrag, true);
}

MainWindow::~MainWindow()
{
    // Сохранение настроек
    QSettings settings;
    settings.setValue("updateGraph", ui->graphUpdateCheckBox->isChecked());
    settings.setValue("grpaphUpdateTime", ui->graphUpdateSpinBox->value());
    settings.setValue("writeLog", ui->writeLogCheckBox->isChecked());
    settings.setValue("writeLogTime", ui->logTimeSpinBox->value());
    settings.setValue("logFile", ui->logFileLineEdit->text());
    settings.setValue("showLastSteps", ui->showLastStepsCheckBox->isChecked());
    settings.setValue("stepsViewCount", ui->stepsViewCountSpinBox->value());
    delete ui;
}

void MainWindow::updatePlot()
{
    // Проверяем поставлена галка обновлять график или нет
    if(ui->graphUpdateCheckBox->isChecked())
    {
        QCustomPlot* customPlot = ui->qcustomplot;
        // Если единицы измерения изменились то очищаем график и обновляем подпись оси Y
        if(mLastUnits != mMultimeter.units())
        {
            customPlot->graph()->clearData();
            mLastUnits = mMultimeter.units();
            customPlot->yAxis->setLabel(QString("Значение, %1").arg(mMultimeter.getUnitName(mMultimeter.units())));
        }
        double time = QDateTime::currentDateTime().toTime_t();
        // Добавляем точку к графику
        customPlot->graph()->addData(time, mMultimeter.isOnline()?mMultimeter.value():std::numeric_limits<double>::quiet_NaN());
        customPlot->rescaleAxes();
        if(ui->showLastStepsCheckBox->isChecked())
        {
            customPlot->xAxis->setRange(time - ui->stepsViewCountSpinBox->value() * ui->graphUpdateSpinBox->value(), time);
        }
        customPlot->replot();
    }
    // Обновляем метку состояния
    if(mMultimeter.isOnline())
    {
        ui->statusLabel->setText(QString("Измерение - %1 %2 %3")
                                 .arg(mMultimeter.getModeName(mMultimeter.mode()))
                                 .arg(mMultimeter.value())
                                 .arg(mMultimeter.getUnitName(mMultimeter.units()))
                                 );
    }
    else
    {
        ui->statusLabel->setText("Выключено");
    }
}

void MainWindow::updateLog()
{
    // Проверяем поставлена галка обновлять лог или нет
    if(ui->writeLogCheckBox->isChecked())
    {
        // Открываем файл
        QFile file(ui->logFileLineEdit->text());
        if(file.open(QIODevice::Append))
        {
            QTextStream stream(&file);
            // Если файл пустой то добавляем шапку
            if(file.size() ==  0)
            {
                stream << trUtf8("Время;Значение;Единицы;\r\n");
            }
            // Добавляем время
            stream << QDateTime::currentDateTime().toString("hh:mm:ss") << ";";
            // Если мултиметр подключен то добавляем данные, иначе добавляем пустые значения
            if(mMultimeter.isOnline())
            {
                stream << QString::number(mMultimeter.value()) << ";" << mMultimeter.getUnitName(mMultimeter.units()) << ";\r\n";
            }
            else
            {
                stream << "-;-;\r\n";
            }
            // Закрываем файл
            file.close();
        }
    }
}

void MainWindow::on_graphUpdateSpinBox_valueChanged(int arg1)
{
    // Устанавливаем интервал обновления графика
    mGraphUpdateTimer.setInterval(arg1 * 1000);
}

void MainWindow::on_action_triggered()
{
    // Запрашиваем доступные порты
    QStringList ports = mMultimeter.availablePorts();
    // Если доступных портов нет то выводим сообщение, иначе запрашиваем порт для подключения
    if(ports.count() == 0)
    {
        QMessageBox::warning(this, "Нет портов", "В системе не найдено ни одного порта");
    }
    else
    {
        mMultimeter.setPort(QInputDialog::getItem(this, "Выберите порт", "Порт", ports));
    }
}

void MainWindow::on_logTimeSpinBox_valueChanged(int arg1)
{
    // Устанавливаем интервал обновления лога
    mLogUpdateTimer.setInterval(arg1 * 1000);
}

void MainWindow::on_selectlogFilePushButton_clicked()
{
    // Вызываем диалог выбора файла для логирования
    QFileDialog fileDialog;
    fileDialog.setFileMode(QFileDialog::AnyFile);
    fileDialog.setDefaultSuffix("csv");
    fileDialog.setWindowTitle(trUtf8("Сохранить лог"));
    fileDialog.setNameFilter("Comma selected value (*.csv)");
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    if (fileDialog.exec() == QFileDialog::Accepted)
    {
        ui->logFileLineEdit->setText(fileDialog.selectedFiles().first());
    }
}

void MainWindow::on_exitAction_triggered()
{
    close();
}

void MainWindow::on_action_2_triggered()
{
    ui->qcustomplot->graph()->clearData();
}

void MainWindow::on_action_3_triggered()
{
    ui->graphUpdateCheckBox->setChecked(true);
    ui->graphUpdateSpinBox->setValue(1);
    ui->writeLogCheckBox->setChecked(true);
    ui->logTimeSpinBox->setValue(1);
    ui->logFileLineEdit->setText("log.csv");
    ui->showLastStepsCheckBox->setChecked(true);
    ui->stepsViewCountSpinBox->setValue(10);
}

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("X");
    QCoreApplication::setApplicationName("Multimeter");
    MainWindow w;
    w.show();

    return a.exec();
}
